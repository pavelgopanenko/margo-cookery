FROM nginx:1.10.3-alpine
MAINTAINER Pavel Gopanenko <pavelgopanenko@gmail.com>

EXPOSE 80

# @UGLY Fix me
COPY ./static /usr/share/nginx/html/static

COPY ./static /usr/share/nginx/html/edu/2017/cookery/static
COPY ./dist /usr/share/nginx/html/edu/2017/cookery/static
COPY ./index.html /usr/share/nginx/html/edu/2017/cookery
