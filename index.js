
'use strict';

import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/moon.css';

import Reveal from 'reveal.js';

Reveal.initialize();
