# Призентация «Кулинарные традиции разных стран»

![CC-BY-NC](https://licensebuttons.net/l/by-nc/2.0/88x31.png)
[![build status](https://gitlab.com/pavelgopanenko/margo-cookery/badges/master/build.svg)](https://gitlab.com/pavelgopanenko/margo-cookery/commits/master)

## Что это?

Это продолжение серии проектов «Видимость Образования».
На этот раз нужно подготовить презентацию на тему «Кулинарные традиции разных стран», кроме того еще приготовить какое-либо блюдо.

Естественно, что такое «презентация» ребенок понятия не имеет. По этому роли разделились так: ребенок на кулинарных работах, супруга - продакт-менеджер, я за IT составляющую.

На этот раз презентация работает в вебе. На борту Webpack, собирается в GitLab'е в docker-образ и деплоиться контейнером.

Результаты [можно найти тут](http://margo.gopanenko.ru/edu/2017/cookery)

## Как запустить локально?

```$ docker run --rm -w /app -v $PWD:/app --entrypoint /bin/bash -v 3000:3000 node```

```$ npm install ```

```$ ENV_NODE=development npm run-script start```

На хосте можно открывать бразуер на [localhost:3000](http://localhost:3000)

## Лицензия

[Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0)](https://creativecommons.org/licenses/by-nc/2.0/)
